const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const demoSchema = new Schema({
	title: {
		type: String,
		required: true
	},
	author: {
		type: String,
		required: true
	},
	year: {
		type: Number,
		required: true
	},
	pages: {
		type: Number,
		required: true, min: 1
	},
	createdAt: {
		type: Date,
		default: Date.now
	}
});

demoSchema.pre('save', next => {
	if (!this.createdAt) {
		this.createdAt = new Date();
	}

	next();
});

module.exports = mongoose.model('DemoObject', demoSchema);
