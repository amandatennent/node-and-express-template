const express = require('express');
const router = express.Router();
const demoController = require('../controllers/demoController');

router.get('/', (req, res) => {
	res.send('Hey! It works!');
});

router.route('/demo_data')
	.get(demoController.get_data)
	.post(demoController.post_data);

router.route('/demo_data/:id')
	.get(demoController.get_object_data)
	.delete(demoController.delete_object_data)
	.put(demoController.update_object_data);

router.route('/demo_display')
	.get(demoController.get_display);

module.exports = router;
