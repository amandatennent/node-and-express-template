const mongoose = require('mongoose');
const config = require('config');

// Connect to our Database and handle any bad connections
mongoose.connect(config.DBHost);
mongoose.Promise = global.Promise;
mongoose.connection.on('error', (err) => {
	console.error(err.message);
});

const app = require('./app');
app.set('port', config.PORT || 7777);
const server = app.listen(app.get('port'), () => {
	console.info(`Express running → PORT ${server.address().port}`);
});

module.exports = app;
