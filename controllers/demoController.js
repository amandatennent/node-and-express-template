const DemoObject = require('../models/Demo');

//  GET /demo_data to retrieve all objects and display as JSON object
exports.get_data = (req, res) => {
	const query = DemoObject.find({});
	query.exec((err, demoObjects) => {
		if (err)
			res.send(err);
		else
			res.json(demoObjects);
	});
};

// POST /demo_data to save a new object
exports.post_data = (req, res) => {
	const newObject = new DemoObject(req.body);

	newObject.save((err, demoObject) => {
		if (err)
			res.send(err);
		else
			res.json({
				message: 'Object successfully added',
				demoObject: demoObject
			});
	});
};

// GET /demo_data/:id route to retrieve an object by it's ID
exports.get_object_data = (req, res) => {
	DemoObject.findById(req.params.id, (err, demoObject) => {
		if (err)
			res.send(err);
		else
			res.json(demoObject);
	});
};

// DELETE /demo_data:id route to delete a book given it's ID
exports.delete_object_data = (req, res) => {
	DemoObject.deleteOne({ _id: req.params.id }, (err, result) => {
		if (err)
			res.send(err);
		res.json({
			message: 'Object successfully deleted',
			result: result
		});
	});
};

// PUT /demo_data/:id route to update an object given it's ID
exports.update_object_data = (req, res) => {
	DemoObject.findById({ _id: req.params.id }, (err, object) => {
		if (err)
			res.send(err);
		else
			Object.assign(object, req.body).save((err, demoObject) => {
				if (err)
					res.send(err);
				else
					res.json({
						message: 'Object updated',
						demoObject: demoObject
					});
			});
	});
};


//  GET /demo_display to retrieve all objects and display formatted on web page
exports.get_display = (req, res) => {
	const query = DemoObject.find({});
	query.exec((err, demoObjects) => {
		if (err) {
			const errorDetails = {
				message: err.message,
				status: err.status,
				stackHighlighted: err.stack.replace(/[a-z_-\d]+.js:\d+:\d+/gi, '<mark>$&</mark>')
			};

			res.render('error', errorDetails);
		}
		else
			res.render('demo', { title: 'Display All Objects', data: demoObjects });
	});
};
