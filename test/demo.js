process.env.NODE_ENV = 'test';

// const mongoose = require('mongoose');
const DemoObject = require('../models/Demo');

//Require the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../start');
const mocha = require('mocha');
const describe = mocha.describe;
const beforeEach = mocha.beforeEach;
const it = mocha.it;
const should = chai.should();

chai.use(chaiHttp);

const request = chai.request(server).keepOpen();

//Our parent block
describe('Demo Objects', () => {
	beforeEach((done) => { //Before each test we empty the database
		DemoObject.deleteMany({}, (err) => {
			if (err)
				console.error(err);
			done();
		});
	});

	/* Test the /GET route */
	describe('/GET demo object', () => {
		it('it should GET all the objects', (done) => {
			request.get('/demo_data')
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('array');
					res.body.length.should.be.eql(0);
					done();
				});
		});
	});

	/* Test the /POST route */
	describe('/POST demo object', () => {
		it('it should not POST an object without pages field', (done) => {
			const demo_object = {
				title: 'The Lord of the Rings',
				author: 'J.R. Tolkien',
				year: 1954
			};
			request.post('/demo_data')
				.send(demo_object)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('object');
					res.body.should.have.property('errors');
					res.body.errors.pages.should.have.property('kind').eql('required');
					done();
				});
		});
		it('it should POST a demo object', (done) => {
			const demo_object = {
				title: 'The Lord of the Rings',
				author: 'J.R. Tolkien',
				year: 1954,
				pages: 1170
			};
			request.post('/demo_data')
				.send(demo_object)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('object');
					res.body.demoObject.should.have.property('title');
					res.body.demoObject.should.have.property('author');
					res.body.demoObject.should.have.property('pages');
					res.body.demoObject.should.have.property('year');
					done();
				});
		});
	});

	/* Test the /GET/:id route */
	describe('/GET/:id demo object', () => {
		it('it should GET an object by the given id', (done) => {
			const demo_object = new DemoObject({
				title: 'The Lord of the Rings',
				author: 'J.R. Tolkien',
				year: 1954,
				pages: 1170
			});
			demo_object.save((err, object) => {
				if (err)
					console.error(err);
				request.get('/demo_data/' + object.id)
					.send(object)
					.end((err, res) => {
						res.should.have.status(200);
						res.body.should.be.a('object');
						res.body.should.have.property('title');
						res.body.should.have.property('author');
						res.body.should.have.property('pages');
						res.body.should.have.property('year');
						res.body.should.have.property('_id').eql(object.id);
						done();
					});
			});
		});
	});

	/* Test the /PUT/:id route */
	describe('/PUT/:id object', () => {
		it('it should UPDATE an object given the id', (done) => {
			const demo_object = new DemoObject({
				title: 'The Chronicles of Narnia',
				author: 'C.S. Lewis',
				year: 1948,
				pages: 778
			});
			demo_object.save((err, object) => {
				request.put('/demo_data/' + object.id)
					.send({
						title: 'The Chronicles of Narnia',
						author: 'C.S. Lewis',
						year: 1950,
						pages: 778
					})
					.end((err, res) => {
						res.should.have.status(200);
						res.body.should.be.a('object');
						res.body.should.have.property('message').eql('Object updated');
						res.body.demoObject.should.have.property('year').eql(1950);
						done();
					});
			});
		});
	});

	/* Test the /DELETE/:id route */
	describe('/DELETE/:id', () => {
		it('it should DELETE an object given the id', (done) => {
			const demo_object = new DemoObject({
				title: 'The Chronicles of Narnia',
				author: 'C.S. Lewis',
				year: 1948,
				pages: 778
			});
			demo_object.save((err, object) => {
				request.delete('/demo_data/' + object.id)
					.end((err, res) => {
						res.should.have.status(200);
						res.should.be.a('object');
						res.body.should.have.property('message').eql('Object successfully deleted');
						res.body.result.should.have.property('ok').eql(1);
						res.body.result.should.have.property('n').eql(1);
						done();
					});
			});
		});
	});
});
